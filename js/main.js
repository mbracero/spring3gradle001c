var app = angular.module('myApp', []);

app.controller('SimpleAjaxController', function($scope, $http) {

	var fireGet = function() {
    var feedUrl = "http://localhost:9000/api/items";

    $http.get(feedUrl)
    .success(function (data) {
      $scope.resultGet = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultGet = data;
    });
  };

  $scope.callGet = function() {
    fireGet();
  };



/***********************************************************************************************************************************/


	var fireGetById = function(id) {
    var feedUrl = "http://localhost:9000/api/items/" + id;

    $http.get(feedUrl)
    .success(function (data) {
      $scope.resultGetById = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultGetById = data;
    });
  };

  $scope.callGetById = function(id) {
    fireGetById(id);
  };



/***********************************************************************************************************************************/


  var fireHttpPostJsonToJson = function() {
    var feedUrl = "http://localhost:9000/api/items.json";

    var req = {
      id: $scope.id,
      name: $scope.name
    };

    $http.post(feedUrl, req)
    .success(function (data) {
      $scope.resultHttpPostJsonToJson = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultHttpPostJsonToJson = data;
    });
  };

  $scope.callHttpPostJsonToJson = function() {
    fireHttpPostJsonToJson();
  };


/***********************************************************************************************************************************/


  var fireHttpPostJsonToForm = function() {
    var feedUrl = "http://localhost:9000/api/items.form";

    var req = {
      id: $scope.id,
      name: $scope.name
    };

    $http.post(feedUrl, req)
    .success(function (data) {
      $scope.resultHttpPostJsonToForm = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultHttpPostJsonToForm = data;
    });
  };

  $scope.callHttpPostJsonToForm = function() {
    fireHttpPostJsonToForm();
  };


/***********************************************************************************************************************************/


  var fireHttpPostFormToJson = function() {
    var feedUrl = "http://localhost:9000/api/items.json";

    var _data = {
      id: $scope.id,
      name: $scope.name
    };

    var req = {
      method: 'POST',
      url: feedUrl,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: _data,
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    };

    $http(req)
    .success(function (data) {
      $scope.resultHttpPostFormToJson = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultHttpPostFormToJson = data;
    });
  };

  $scope.callHttpPostFormToJson = function() {
    fireHttpPostFormToJson();
  };


/***********************************************************************************************************************************/


  var fireHttpPostFormToForm = function() {
    var feedUrl = "http://localhost:9000/api/items.form";

    var _data = {
      id: $scope.id,
      name: $scope.name
    };

    var req = {
      method: 'POST',
      url: feedUrl,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: _data,
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    };

    $http(req)
    .success(function (data) {
      $scope.resultHttpPostFormToForm = data;
    })
    .error(function (data, status, headers, config) {
    	$scope.resultHttpPostFormToForm = data;
    });
  };

  $scope.callHttpPostFormToForm = function() {
    fireHttpPostFormToForm();
  };


/***********************************************************************************************************************************/


  var fireAjaxJson = function() {
    var req = {
      id: $scope.id,
      name: $scope.name
    };

	  $.ajax({
	      type: "POST",
	      url: "http://localhost:9000/api/items.json",
	      // The key needs to match your method's input parameter (case-sensitive).
	      data: JSON.stringify(req),
	      contentType: "application/json; charset=utf-8",
	      dataType: "json",
	      success: function(data){
	      	$scope.$apply(function () {
            $scope.resultAjaxJson = data;
        	});
	      },
	      failure: function(errMsg) {
	      	$scope.$apply(function () {
            $scope.resultAjaxJson = errMsg;
        	});
	      }
	  });
  };

  $scope.callAjaxJson = function() {
    fireAjaxJson();
  };



/***********************************************************************************************************************************/



  var fireAjaxUrlencoded = function() {
    var req = {
      id: $scope.id,
      name: $scope.name
    };

	  var str = [];
	    for(var p in req)
	      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(req[p]));
	  var reqResult = str.join("&");

	  $.ajax({
	      type: "POST",
	      url: "http://localhost:9000/api/items.form",
	      // The key needs to match your method's input parameter (case-sensitive).
	      data: reqResult,
	      contentType: "application/x-www-form-urlencoded",
	      success: function(data){
	      	$scope.$apply(function () {
            $scope.resultAjaxUrlencoded = data;
        	});
	      },
	      failure: function(errMsg) {
	      	$scope.$apply(function () {
            $scope.resultAjaxUrlencoded = errMsg;
        	});
	      }
	  });
  }

  $scope.callAjaxUrlencoded = function() {
    fireAjaxUrlencoded();
  }

  
});

